package com.burbaka.A010_Listener;

import android.app.Activity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

public class MyActivity extends Activity implements View.OnClickListener{
    Button btnOk;
    Button btnCancel;
    TextView textViewClickedButton;

    /**
     * Called when the activity is first created.
     */
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.main);

        btnOk = (Button) findViewById(R.id.btnOk);
        btnCancel = (Button) findViewById(R.id.btnCancel);
        textViewClickedButton = (TextView) findViewById(R.id.tvOut);

        btnOk.setOnClickListener(this);
        btnCancel.setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.btnOk:
                textViewClickedButton.setText("Ok Button clicked");
                break;
            case R.id.btnCancel:
                textViewClickedButton.setText("Cancel Button clicked");
                break;
        }
    }
}
