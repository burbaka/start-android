package com.burbaka.A040_LayoutInflater;

import android.app.Activity;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

public class MyActivity extends Activity {
    final String TAG = "myLogs";

    /**
     * Called when the activity is first created.
     */
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.main);

        LayoutInflater layoutInflater = getLayoutInflater();
        LinearLayout linearLayout = (LinearLayout) findViewById(R.id.linLayout);
        View view = layoutInflater.inflate(R.layout.text, linearLayout, true);
        ViewGroup.LayoutParams layoutParams = view.getLayoutParams();

        Log.i(TAG, "Class of view: = " + view.getClass().toString());
        Log.i(TAG, "Class of layoutParams of view: " + layoutParams.getClass().toString());

        RelativeLayout relativeLayout = (RelativeLayout) findViewById(R.id.relLayout);
        View anotherView = layoutInflater.inflate(R.layout.text, relativeLayout, true);
        ViewGroup.LayoutParams layoutParamsForAnotherView = anotherView.getLayoutParams();

        Log.i(TAG, "Class of anotherView: = " + anotherView.getClass().toString());
        Log.i(TAG, "Class of layoutParams of anotherView: " + layoutParamsForAnotherView.getClass().toString());
    }
}
