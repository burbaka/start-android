package com.burbaka.A011_ResValues;

import android.app.Activity;
import android.os.Bundle;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.TextView;

public class MyActivity extends Activity {
    /**
     * Called when the activity is first created.
     */
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.main);

        LinearLayout linearLayoutBottom = (LinearLayout) findViewById(R.id.llBottom);
        Button btnBottom = (Button) findViewById(R.id.btnBottom);
        TextView textViewBottom = (TextView) findViewById(R.id.tvBottom);

        linearLayoutBottom.setBackgroundResource(R.color.llBottomColor);
        btnBottom.setText(R.string.btnBottomText);
        textViewBottom.setText(R.string.textViewBottomText);
    }
}
