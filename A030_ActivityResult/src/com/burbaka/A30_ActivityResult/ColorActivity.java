package com.burbaka.A30_ActivityResult;

import android.app.Activity;
import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import com.burbaka.A30_ActivityResult.R;

/**
 * Created with IntelliJ IDEA.
 * User: Oleksandr
 * Date: 16.01.13
 * Time: 11:03
 * To change this template use File | Settings | File Templates.
 */
public class ColorActivity extends Activity implements View.OnClickListener {
    private static final String TAG = "myLogs";
    private Button btnBlueColor;
    private Button btnGreenColor;
    private Button btnRedColor;


    public void onCreate(Bundle savedInstanceState){
        Log.i(TAG, "onCreate in ColorActivity was called");
        super.onCreate(savedInstanceState);
        setContentView(R.layout.color_activity_layout);

        btnBlueColor = (Button) findViewById(R.id.btnBlue);
        btnGreenColor = (Button) findViewById(R.id.btnGreen);
        btnRedColor = (Button) findViewById(R.id.btnRed);

        btnRedColor.setOnClickListener(this);
        btnGreenColor.setOnClickListener(this);
        btnBlueColor.setOnClickListener(this);
    }




    @Override
    public void onClick(View v) {
        Intent intent = new Intent();
        switch (v.getId()){
            case R.id.btnBlue:
                intent.putExtra("color", Color.BLUE);
            break;
            case R.id.btnGreen:
                intent.putExtra("color", Color.GREEN);
            break;
            case R.id.btnRed:
                intent.putExtra("color", Color.RED);
            break;
        }
        setResult(RESULT_OK, intent);
        finish();
    }
}
