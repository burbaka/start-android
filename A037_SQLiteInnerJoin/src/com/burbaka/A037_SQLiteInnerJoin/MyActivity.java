package com.burbaka.A037_SQLiteInnerJoin;

import android.app.Activity;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.os.Bundle;
import android.util.Log;

public class MyActivity extends Activity {
    final public static String TAG = "myLogs";

    @Override
    public void onResume(){
        super.onResume();

        Log.d(TAG, "onResume started()");
        // Подключаемся к БД
        DBHelper dbHelper = new DBHelper(this);
        SQLiteDatabase db = dbHelper.getWritableDatabase();

        Log.d(TAG, "database creation finished");
        // Описание курсора
        Cursor cursor;

        // выводим в лог данные по должностям
        Log.i(TAG, "--- Table position ---");
        cursor = db.query("position", null, null, null, null, null, null);
        logCursor(cursor);
        cursor.close();
        Log.i(TAG, "--- ---");

        // выводим в лог данные по людям
        Log.i(TAG, "--- Table people ---");
        cursor = db.query("people", null, null, null, null, null, null);
        logCursor(cursor);
        cursor.close();
        Log.d(TAG, "--- ---");

        // выводим результат объединения
        // используем rawQuery
        Log.i(TAG, "--- INNER JOIN with rawQuery ---");
        String sqlQuery = "select PL.name as Name, PS.name as Position, salary as Salary "
                + "from people as PL "
                + "inner join position as PS "
                + "on PL.posid = PS.id "
                + "where salary > ?";
        cursor = db.rawQuery(sqlQuery, new String[]{"12000"});
        logCursor(cursor);
        cursor.close();
        Log.i(TAG, "--- ---");

        // выводим результат объединения
        // используем query
        Log.i(TAG, "--- INNER JOIN with query ---");
        String table = "people as PL inner join position as PS on PL.posid = PS.id";
        String colums[] = {"PL.name as Name", "PS.name as Position", "salary as Salary"};
        String selection = "salary < ?";
        String[] selectionArgs = {"12000"};
        cursor = db.query(table, colums, selection, selectionArgs, null, null, null);
        logCursor(cursor);
        cursor.close();
        Log.i(TAG, "--- ---");
    }



    /**
     * Called when the activity is first created.
     */
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.main);


    }

    // вывод в лог данных из курсора
    private void logCursor(Cursor cursor) {
        if (cursor != null) {
            if (cursor.moveToFirst()) {
                String str;
                do {
                    str = "";
                    for (String cn : cursor.getColumnNames()) {
                        str = str.concat(cn + " = " + cursor.getString(cursor.getColumnIndex(cn)) + "; ");
                    }
                    Log.i(TAG, str);
                } while (cursor.moveToNext());
            } else {
                Log.i(TAG, "Selection is empty");
            }
        } else {
            Log.i(TAG, "Cursor is null");
        }
    }
}
