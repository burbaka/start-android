package com.burbaka.A019_SimpleCalculator;

import android.app.Activity;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

public class MyActivity extends Activity implements View.OnClickListener {
    final int MENU_RESET_ID = 1;
    final int MENU_QUIT_ID = 2;

    EditText editTextFirstNumber;
    EditText editTextSecondNumber;
    TextView textViewResult;
    Button btnSum;
    Button btnProduct;
    Button btnDevide;
    Button btnSubstract;

    /**
     * Called when the activity is first created.
     */
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.main);

        editTextFirstNumber = (EditText) findViewById(R.id.etNum1);
        editTextSecondNumber = (EditText) findViewById(R.id.etNum2);
        btnSum = (Button) findViewById(R.id.btnAdd);
        btnDevide = (Button) findViewById(R.id.btnDiv);
        btnProduct = (Button) findViewById(R.id.btnMult);
        btnSubstract = (Button) findViewById(R.id.btnSub);
        textViewResult = (TextView) findViewById(R.id.tvResult);

        btnSum.setOnClickListener(this);
        btnSubstract.setOnClickListener(this);
        btnProduct.setOnClickListener(this);
        btnDevide.setOnClickListener(this);
    }

    public boolean onCreateOptionsMenu(Menu menu) {
        menu.add(0, MENU_RESET_ID, 0, "Reset");
        menu.add(0, MENU_QUIT_ID, 0, "Quit");
        return super.onCreateOptionsMenu(menu);
    }

    public boolean onOptionsItemsSelected(MenuItem item){
        switch (item.getItemId()){
            case MENU_RESET_ID:
                editTextSecondNumber.setText("");
                editTextSecondNumber.setText("");
            break;
            case MENU_QUIT_ID:
                finish();
            break;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onClick(View v) {
        double result = 0;
        double firstNumber = 0;
        double secondNumber = 0;

        if (TextUtils.isEmpty(editTextSecondNumber.getText().toString()) || TextUtils.isEmpty(editTextSecondNumber.getText().toString())) {
            return;
        }

        firstNumber = Double.parseDouble(editTextFirstNumber.getText().toString());
        secondNumber = Double.parseDouble(editTextSecondNumber.getText().toString());
        switch (v.getId()) {
            case R.id.btnAdd:
                result = firstNumber + secondNumber;
                break;
            case R.id.btnSub:
                result = firstNumber - secondNumber;
                break;
            case R.id.btnMult:
                result = firstNumber * secondNumber;
                break;
            case R.id.btnDiv:
                if (secondNumber == 0) {
                    textViewResult.setText("Invalid math operation");
                    return;
                }
                result = firstNumber / secondNumber;
                break;
        }
        textViewResult.setText(String.valueOf(result));
    }
}
