package com.burbaka.A026_IntentFilter;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

public class MyActivity extends Activity implements View.OnClickListener{
    Button btnShowDateActivity;
    Button btnShowTimeActivity;

    /**
     * Called when the activity is first created.
     */
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.main);

        btnShowDateActivity = (Button) findViewById(R.id.btnDate);
        btnShowTimeActivity = (Button) findViewById(R.id.btnTime);

        btnShowTimeActivity.setOnClickListener(this);
        btnShowDateActivity.setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
        Intent intent = null;
        switch (v.getId()) {
            case R.id.btnDate:
                intent = new Intent("ru.startandroid.intent.action.showdate");
            break;
            case R.id.btnTime:
                intent = new Intent("ru.startandroid.intent.action.showtime");
            break;
            default:
            break;
        }
        startActivity(intent);
    }
}
