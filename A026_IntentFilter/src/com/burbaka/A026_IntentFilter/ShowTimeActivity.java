package com.burbaka.A026_IntentFilter;

import android.app.Activity;
import android.os.Bundle;
import android.util.Log;
import android.widget.TextView;

import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * Created with IntelliJ IDEA.
 * User: Oleksandr
 * Date: 16.01.13
 * Time: 7:01
 */
public class ShowTimeActivity extends Activity {
    private final String TAG = "myLogs";

    @Override
    public void onCreate(Bundle savedInstanceState){
        Log.i(TAG, "onCreate started.");
        super.onCreate(savedInstanceState);
        setContentView(R.layout.show_time_layout);

        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("HH:mm:ss");
        String time = simpleDateFormat.format(new Date(System.currentTimeMillis()));

        TextView textView = (TextView) findViewById(R.id.tvTime);
        textView.setText(time);
    }
}
