package com.burbaka.A026_IntentFilter;

import android.app.Activity;
import android.os.Bundle;
import android.util.Log;
import android.widget.TextView;

import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * Created with IntelliJ IDEA.
 * User: Oleksandr
 * Date: 16.01.13
 * Time: 6:58
 * To change this template use File | Settings | File Templates.
 */
public class ShowDateActivity extends Activity {

    final private String TAG = "myLogs";

    @Override
    public void onCreate(Bundle savedInstanceState){

        Log.i(TAG,"onCreate in ShowDateActivity started.");
        super.onCreate(savedInstanceState);
        setContentView(R.layout.show_date_layout);

        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("dd:MM:yyyy");
        String date = simpleDateFormat.format(new Date(System.currentTimeMillis()));

        Log.d(TAG, "set TextView in simple date model");
        TextView textView = (TextView) findViewById(R.id.tvDate);
        textView.setText(date);
    }
}
