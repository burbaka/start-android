package com.burbaka.A038_SQLiteTransaction;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

/**
 * Created with IntelliJ IDEA.
 * User: Oleksandr
 * Date: 05.02.13
 * Time: 16:06
 */
public class DBHelper extends SQLiteOpenHelper{
    public DBHelper(Context context) {
        super(context, "myDB", null, 1);
    }

    @Override
    public void onCreate(SQLiteDatabase database) {
        Log.i(MyActivity.TAG, "--- onCreate database ---");
        database.execSQL("create table mytable ("
                + "id integer primary key autoincrement, "
                + "val text"
                + ");");
    }

    @Override
    public void onUpgrade(SQLiteDatabase database, int oldVersion, int newVersion) {
    }
}
