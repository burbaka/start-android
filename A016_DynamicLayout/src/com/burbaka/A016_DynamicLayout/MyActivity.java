package com.burbaka.A016_DynamicLayout;

import android.view.Gravity;
import android.view.View;
import android.view.ViewGroup.*;
import android.app.Activity;
import android.os.Bundle;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.TextView;

public class MyActivity extends Activity {
    /**
     * Called when the activity is first created.
     */
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        LinearLayout linearLayout = new LinearLayout(this);
        linearLayout.setOrientation(LinearLayout.VERTICAL);
        LayoutParams layoutParams = new LayoutParams(LayoutParams.MATCH_PARENT, LayoutParams.MATCH_PARENT);
        setContentView(linearLayout, layoutParams);

        //THis line was added
        LayoutParams lpView = new LayoutParams(LayoutParams.WRAP_CONTENT, LayoutParams.WRAP_CONTENT);

        TextView textView = new TextView(this);
        textView.setText("TextView");
        textView.setLayoutParams(lpView);
        linearLayout.addView(textView);

        Button btn = new Button(this);
        btn.setText("Button");
        linearLayout.addView(btn, lpView);

        LinearLayout.LayoutParams marginLayoutParams = new LinearLayout.LayoutParams(LayoutParams.WRAP_CONTENT, LayoutParams.WRAP_CONTENT);
        marginLayoutParams.leftMargin = 50;
        Button btn1 = new Button(this);
        btn1.setText("Button");
        btn1.setVisibility(View.VISIBLE);
        linearLayout.addView(btn1, marginLayoutParams);

        LinearLayout.LayoutParams rightGravityParams = new LinearLayout.LayoutParams(LayoutParams.WRAP_CONTENT, LayoutParams.WRAP_CONTENT);
        rightGravityParams.gravity = Gravity.RIGHT;

        Button btn2 = new Button(this);
        btn2.setText("Button2");
        linearLayout.addView(btn2, rightGravityParams);
    }
}
