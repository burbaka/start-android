package com.burbaka.A049_SimpleAdapterCustom_Part1;

import android.app.Activity;
import android.os.Bundle;
import android.widget.ListView;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

public class MyActivity extends Activity {
    // имена атрибутов для Map
    final String ATTRIBUTE_NAME_TEXT = "text";
    final String ATTRIBUTE_NAME_VALUE = "value";
    final String ATTRIBUTE_NAME_IMAGE = "image";

    // картинки для отображения динамики
    final public static int POSITIVE = android.R.drawable.arrow_up_float;
    final public static int NEGATIVE = android.R.drawable.arrow_down_float;

    ListView listView;

    /**
     * Called when the activity is first created.
     */
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.main);

        // массив данных
        int[] values = {8, 4, -3, 2, -5, 0, 3, -6, 1, -1};

        // упаковываем данные в понятную для адаптера структуру
        ArrayList<Map<String, Object>> data = new ArrayList<Map<String, Object>>(values.length);
        Map<String, Object> map;
        int image = 0;

        for (int i = 0; i < values.length; i++) {
            map = new HashMap<String, Object>();
            map.put(ATTRIBUTE_NAME_TEXT, "Day " + (i + 1));
            map.put(ATTRIBUTE_NAME_VALUE, values[i]);
            if (values[i] == 0) {
                image = 0;
            } else {
                image = (values[i] > 0) ? POSITIVE : NEGATIVE;
                map.put(ATTRIBUTE_NAME_IMAGE, image);
                data.add(map);
            }
        }

        // массив имен атрибутов, из которых будут читаться данные
        String[] from = {ATTRIBUTE_NAME_TEXT, ATTRIBUTE_NAME_VALUE, ATTRIBUTE_NAME_IMAGE};
        // массив ID View-компонентов, в которые будут вставлять данные
        int[] to = {R.id.tvText, R.id.tvValue, R.id.ivImg};

        // создаем адаптер
        MySimpleAdapter adapter = new MySimpleAdapter(this, data, R.layout.list_item, from, to);

        // определяем список и присваиваем ему адаптер
        listView = (ListView) findViewById(R.id.lvSimple);
        listView.setAdapter(adapter);


    }
}
