package com.burbaka.A049_SimpleAdapterCustom_Part1;

import android.content.Context;
import android.graphics.Color;
import android.graphics.ImageFormat;
import android.widget.ImageView;
import android.widget.SimpleAdapter;
import android.widget.TextView;

import java.util.List;
import java.util.Map;

/**
 * Created with IntelliJ IDEA.
 * User: Oleksandr
 * Date: 28.01.13
 * Time: 16:41
 */
public class MySimpleAdapter extends SimpleAdapter {
    /**
     * Constructor
     *
     * @param context  The context where the View associated with this SimpleAdapter is running
     * @param data     A List of Maps. Each entry in the List corresponds to one row in the list. The
     *                 Maps contain the data for each row, and should include all the entries specified in
     *                 "from"
     * @param resource Resource identifier of a view layout that defines the views for this list
     *                 item. The layout file should include at least those named views defined in "to"
     * @param from     A list of column names that will be added to the Map associated with each
     *                 item.
     * @param to       The views that should display column in the "from" parameter. These should all be
     *                 TextViews. The first N views in this list are given the values of the first N columns
     *                 in the from parameter.
     */
    public MySimpleAdapter(Context context, List<? extends Map<String, ?>> data, int resource, String[] from, int[] to) {
        super(context, data, resource, from, to);
    }

    public void setViewText(TextView view, String text) {
        // метод супер-класса, который вставляет текст
        super.setViewText(view, text);

        // если нужный нам TextView, то разрисовываем
        if (view.getId() == R.id.tvValue) {
            int i = Integer.parseInt(text);
            if (i < 0) {
                view.setTextColor(Color.RED);
            } else if (i > 0) {
                view.setTextColor(Color.GREEN);
            }
        }
    }





    @Override
    public  void setViewImage(ImageView view, int value) {
        // метод супер-класса
        super.setViewImage(view, value);
        // разрисовываем ImageView
        if (value == MyActivity.NEGATIVE) {
            view.setBackgroundColor(Color.RED);
        } else if (value == MyActivity.POSITIVE){
            view.setBackgroundColor(Color.GREEN);
        }
    }
}
