package com.burbaka.A039_SQLiteOnUpgradeDB;

import android.app.Activity;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.os.Bundle;
import android.util.Log;

public class MyActivity extends Activity {
    final public static String TAG = "myLogs";

    final public static String DB_NAME = "staff"; // имя БД
    final public static int DB_VERSION = 2; // версия БД

    /**
     * Called when the activity is first created.
     */
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.main);

        DBHelper dbHelper = new DBHelper(this);
        SQLiteDatabase database = dbHelper.getWritableDatabase();
        Log.d(TAG, " --- Staff database.v"+ database.getVersion() + " --- ");
        writeStaff(database);
        dbHelper.close();
    }

    // запрос данных и вывод в лог
    private void writeStaff(SQLiteDatabase database) {
        Cursor cursor = database.rawQuery("select * from people" , null);
        logCursor(cursor, "Table people");
        cursor.close();

        cursor = database.rawQuery("select * from position", null);
        logCursor(cursor, "Table position");
        cursor.close();

        String sqlQuery = "select PL.name as Name, PS.name as Position, salary as Salary "
                + "from people as PL "
                + "inner join position as PS "
                + "on PL.posid = PS.id ";
        cursor = database.rawQuery(sqlQuery, null);
        logCursor(cursor, "inner join");
        cursor.close();
    }

    // вывод в лог данных из курсора
    private void logCursor(Cursor cursor, String title) {
        if (cursor != null) {
            if (cursor.moveToFirst()) {
                Log.i(MyActivity.TAG, title + ". " + cursor.getCount() + "rows");
                StringBuilder stringBuilder = new StringBuilder();
                do {
                    stringBuilder.setLength(0);
                    for (String column: cursor.getColumnNames()) {
                        stringBuilder.append(column + " = " + cursor.getString(cursor.getColumnIndex(column)) + "; ");
                    }
                    Log.i(TAG, stringBuilder.toString());
                } while (cursor.moveToNext());
            } else {
                Log.i(TAG, "Database is empty");
            }
        } else {
            Log.i(TAG, title + ". Cursor is null");
        }
    }
}
