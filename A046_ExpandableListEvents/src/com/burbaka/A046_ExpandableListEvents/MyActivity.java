package com.burbaka.A046_ExpandableListEvents;

import android.app.Activity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.ExpandableListView;
import android.widget.SimpleExpandableListAdapter;
import android.widget.TextView;

public class MyActivity extends Activity {
    final String TAG = "myLogs";

    ExpandableListView expandableListView;
    AdapterHelper adapterHelper;
    SimpleExpandableListAdapter adapter;
    TextView textViewInfo;

    /**
     * Called when the activity is first created.
     */
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.main);

        textViewInfo = (TextView) findViewById(R.id.tvInfo);

        // создаем адаптер
        adapterHelper = new AdapterHelper(this);
        adapter = adapterHelper.getAdapter();

        expandableListView = (ExpandableListView) findViewById(R.id.elvMain);
        expandableListView.setAdapter(adapter);

        // нажатие на элемент
        expandableListView.setOnChildClickListener(new ExpandableListView.OnChildClickListener() {
            @Override
            public boolean onChildClick(ExpandableListView parent, View v, int groupPosition, int childPosition, long id) {
                Log.i(TAG, "onChildClick groupPosition = " + groupPosition
                + ", childPosition = " + childPosition + ", id = " + id);
                textViewInfo.setText(adapterHelper.getGroupChildText(groupPosition, childPosition));
                return false;
            }
        });

        // нажатие на группу
        expandableListView.setOnGroupClickListener(new ExpandableListView.OnGroupClickListener() {
            @Override
            public boolean onGroupClick(ExpandableListView parent, View v, int groupPosition, long id) {
                Log.i(TAG, "onGroupClick groupPosition = " + groupPosition + ", id = " + id);
                // блокируем дальнейшую обработку события для группы с позицией 1
                if (groupPosition == 1) return true;

                return false;
            }
        });

        // сворачивание группы
        expandableListView.setOnGroupCollapseListener(new ExpandableListView.OnGroupCollapseListener() {
            @Override
            public void onGroupCollapse(int groupPosition) {
                Log.i(TAG, "onGroupCollapse groupPosition = " + groupPosition);
                textViewInfo.setText("Collapse: " + adapterHelper.getGroupText(groupPosition));
            }
        });

        // разворачивание группы
        expandableListView.setOnGroupExpandListener(new ExpandableListView.OnGroupExpandListener() {
            @Override
            public void onGroupExpand(int groupPosition) {
                Log.i(TAG, "onGroupExpand groupPosition = " + groupPosition);
                textViewInfo.setText("Expand: " + adapterHelper.getGroupText(groupPosition));
            }
        });

        // разворачиваем группу с позицией 2
        expandableListView.expandGroup(2);
    }
}
