package com.burbaka.A018_DynamicLayout3;

import android.app.Activity;
import android.os.Bundle;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.SeekBar;

public class MyActivity extends Activity implements SeekBar.OnSeekBarChangeListener {
    SeekBar seekBarWeight;
    Button btnFirst;
    Button btnSecond;

    LinearLayout.LayoutParams layoutParamsFirst;
    LinearLayout.LayoutParams layoutParamsSecond;

    /**
     * Called when the activity is first created.
     */
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.main);

        seekBarWeight = (SeekBar) findViewById(R.id.sbWeight);
        btnFirst = (Button) findViewById(R.id.btn1);
        btnSecond = (Button) findViewById(R.id.btn2);

        layoutParamsFirst = (LinearLayout.LayoutParams) btnFirst.getLayoutParams();
        layoutParamsSecond = (LinearLayout.LayoutParams) btnSecond.getLayoutParams();

        seekBarWeight.setOnSeekBarChangeListener(this);

    }

    @Override
    public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {
        int leftValue = progress;
        int rightValue = seekBar.getMax() - progress;

        layoutParamsFirst.weight = leftValue;
        layoutParamsSecond.weight = rightValue;

        btnFirst.setText(String.valueOf(leftValue));
        btnSecond.setText(String.valueOf(rightValue));
    }

    @Override
    public void onStartTrackingTouch(SeekBar seekBar) {

    }

    @Override
    public void onStopTrackingTouch(SeekBar seekBar) {

    }
}
