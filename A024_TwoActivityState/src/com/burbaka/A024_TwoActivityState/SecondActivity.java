package com.burbaka.A024_TwoActivityState;

import android.app.Activity;
import android.os.Bundle;
import android.util.Log;

/**
 * Created with IntelliJ IDEA.
 * User: Oleksandr
 * Date: 16.01.13
 * Time: 4:46
 * To change this template use File | Settings | File Templates.
 */
public class SecondActivity extends Activity {
    final private String TAG = "myLogs";
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.second_activity_layout);
    }

    public void onStart(){
        super.onStart();
        Log.i(TAG, "SecondActivity: onStart started.");
    }

    public void onResume(){
        super.onResume();
        Log.i(TAG, "SecondActivity: onResume started.");
    }

    public void onRestart(){
        super.onRestart();
        Log.i(TAG, "SecondActivity: onRestart started.");
    }

    public void onPause(){
        super.onPause();
        Log.i(TAG, "SecondActivity: onPause started.");
    }

    public void onStop(){
        super.onStop();
        Log.i(TAG, "SecondActivity: onStop started.");
    }

    public void onDestroy(){
        super.onDestroy();
        Log.i(TAG, "SecondActivity: onDestroy started.");
    }
}
