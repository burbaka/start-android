package com.burbaka.A024_TwoActivityState;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.Switch;

public class MyActivity extends Activity implements View.OnClickListener{
    final private String TAG = "myLogs";

    Button btnCallActivity;
    /**
     * Called when the activity is first created.
     */
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.main);

        btnCallActivity = (Button) findViewById(R.id.btnActTwo);
        btnCallActivity.setOnClickListener(this);
    }

    public void onStart(){
        super.onStart();
        Log.i(TAG, "MyActivity: onStart started.");
    }

    public void onResume(){
        super.onResume();
        Log.i(TAG, "MyActivity: onResume started.");
    }

    public void onRestart(){
        super.onRestart();
        Log.i(TAG, "MyActivity: onRestart started.");
    }

    public void onPause(){
        super.onPause();
        Log.i(TAG, "MyActivity: onPause started.");
    }

    public void onStop(){
        super.onStop();
        Log.i(TAG, "MyActivity: onStop started.");
    }

    public void onDestroy(){
        super.onDestroy();
        Log.i(TAG, "MyActivity: onDestroy started.");
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case    R.id.btnActTwo:
                Intent intent = new Intent(this, SecondActivity.class);
                startActivity(intent);
            break;
        }
    }
}
