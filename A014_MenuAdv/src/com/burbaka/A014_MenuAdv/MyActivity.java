package com.burbaka.A014_MenuAdv;

import android.app.Activity;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.CheckBox;
import android.widget.TextView;

public class MyActivity extends Activity {

    public static final String TAG = "myLogs";
    // screen elements
    TextView vText;
    CheckBox vCheckBox;


    /** Called when the activity is first created. */
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.main);
        // find elements
        vText = (TextView) findViewById(R.id.textView);
        vCheckBox = (CheckBox) findViewById(R.id.chbExtMenu);

    }

    // menu creation
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        Log.i(TAG, "onCreateOptionsMenu was called");

        getMenuInflater().inflate(R.menu.mymenu, menu);

        return super.onCreateOptionsMenu(menu);
    }

    // menu refreshing
    @Override
    public boolean onPrepareOptionsMenu(Menu menu) {
        Log.i(TAG, "onPrepareOptionsMenu was called");
        // пункты меню с ID группы = 1 видны, если в CheckBox стоит галка
        menu.setGroupVisible(R.id.group1, vCheckBox.isChecked());
        return super.onPrepareOptionsMenu(menu);
    }

    // click events handling
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        Log.i(TAG, "onOptionsItemSelected was called");
        StringBuilder stringBuilder = new StringBuilder();

        // Show in TextView information about menu item which was clicked
        stringBuilder.append("Item Menu");
        stringBuilder.append("\r\n groupId: ").append(String.valueOf(item.getGroupId()));
        stringBuilder.append("\r\n itemId: ").append(String.valueOf(item.getItemId()));
        stringBuilder.append("\r\n order: ").append(String.valueOf(item.getOrder()));
        stringBuilder.append("\r\n title: ").append(item.getTitle());
        vText.setText(stringBuilder.toString());

        return super.onOptionsItemSelected(item);
    }
}