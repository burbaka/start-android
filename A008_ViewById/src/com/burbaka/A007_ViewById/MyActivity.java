package com.burbaka.A007_ViewById;

import android.app.Activity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.TextView;

public class MyActivity extends Activity {
    /**
     * Called when the activity is first created.
     */
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.main);
        TextView myTextView = (TextView) findViewById(R.id.myname);
        Button myBtn = (Button) findViewById(R.id.mybtn);
        CheckBox myCheckBox = (CheckBox) findViewById(R.id.myCheckBox);
        myTextView.setText("New text main activity");
        myBtn.setText("My button new text");
        myBtn.setEnabled(false);
        myCheckBox.setChecked(true);
    }
}
