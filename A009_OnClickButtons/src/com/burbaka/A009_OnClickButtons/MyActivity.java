package com.burbaka.A009_OnClickButtons;

import android.app.Activity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.view.View.OnClickListener;

public class MyActivity extends Activity {
    Button btnOk;
    Button btnCancel;
    TextView txtViewForClickedBtn;

    /**
     * Called when the activity is first created.
     */
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.main);
        btnOk = (Button) findViewById(R.id.btnOk);
        btnCancel = (Button) findViewById(R.id.btnCancel);
        txtViewForClickedBtn = (TextView) findViewById(R.id.tvOut);

        OnClickListener onClickListenerOkButton = new OnClickListener(){
            @Override
            public void onClick(View v){
                txtViewForClickedBtn.setText("Нажата кнопка ОК");
            }
        };
        OnClickListener onClickListenerCancelButton = new OnClickListener() {
            @Override
            public void onClick(View v) {
                txtViewForClickedBtn.setText("Нажата кнопка Cancel");
            }
        };

        btnOk.setOnClickListener(onClickListenerOkButton);
        btnCancel.setOnClickListener(onClickListenerCancelButton);
    }
}
